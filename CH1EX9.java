import java.util.Scanner;
public class CH1EX9 
// Write a short Java Method that uses a StringBuilder instance to remove all the punctuation 
// from a string s storing a sentence, for example, transforming the string "Let's try, Mike!"
// to "Lets try Mike".
{
    // String -> String
    // StringBuilder takes a string and removes all the punctuation from a string and outputs 
    // the sentence.
    // given: Let's try, Mike!; expect: Lets try Mike
    // given: Good job! You rock, Emma!; Good job You rock Emma
    public static void main(String[] args)
    {
        String str;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a string >>");
        str = input.nextLine();
        System.out.print(str);
        input.close();
        System.out.print(StringBuilder(str));
    }
    public static String StringBuilder(String str)
    {
        StringBuilder string = new StringBuilder();
        for (int x = 0; x < str.length(); x++)
        {
            if(Character.isSpaceChar(str.charAt(x)) || Character.isAlphabetic(str.charAt(x)))
            {
                string.append(string.charAt(x));
            }
        }
        return string.toString();
    }
    
}
