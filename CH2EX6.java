import java.util.Scanner;
import java.io.*;
import java.io.PrintStream;
public class CH2EX6
//Give a short fragment of Java code that uses the progression classes
// from Section 2.2.3 to find the eighth value of a Fibonacci progression
// that starts with 2 and 2 as its first two values
{
    public static void main(String[] args)
    {
        FibonacciProgression myProgression = new FibonacciProgression(2,2);
        myProgression.printProgression(8);
    } 
}
class Progression
// generates a simple progression. By default: 0,1,2,...
{
    // instance variable
    protected long current;
    // constructs a progression starting at 0
    public Progression() {this(0);}
    // constructs a progression with given a start value
    public Progression (long start) {current = start;}
    // returns the next value of the progression
    public long nextValue()
    {
        long answer = current;
        advance(); //this protected call is responsible for advancing the current value
        return answer; 
    }
    // advances the current value to the next value of the progression
    protected void advance()
    {
        current++;
    }
    // prints the next n values of the progression, seperated by spaces
    public void printProgression (int n)
    {
        System.out.print(nextValue()); //print first value without leading space
        for (int j = 1; j < n; j++)
            System.out.print(" " + nextValue()); //print leading space before others
        //System.out.printIn(); //end the line
    }
}
class FibonacciProgression extends Progression
{
    protected long prev;
    //constructs traditional Fibonacci, starting 0,1,1,2,3,...
    public FibonacciProgression() {this(0,1);}
    //constructs generalized Fibonacci, with give first and second values
    public FibonacciProgression(long first, long second)
    {
        super(first);
        prev = second - first; //fictitious value preceding the first
    }
    // replaces (prev, current) with (current, current+prev)
    protected void advance()
    {
        long temp = prev;
        prev = current;
        current += temp;
    }
}
