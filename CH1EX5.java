import java.util.Scanner;
public class CH1EX5 
// Write a short Java method that takes an integer n and returns the sum of all positive integers
// less than or equal to n
{
    // Integer -> Integer
    // Main takes an integer n and returns the sum of all positive integers less than or euqal to n
    // given: 5, expect: 15
    // given: 0, expect: 0
    public static void main(String[] args)
    {
        int n;
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter a integer >>");
        n = input.nextInt();
        input.close();
        int sum = 0;
        while (n > 0)
        {
            sum = sum + n;
            n = n - 1;
        }
        System.out.print("The sum of all positive integers less than or equal to n is: " + sum);
    }
}
