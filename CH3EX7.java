public class CH3EX7 
// Consider the implementation of CircularlyLinkedList.addFirst, in Code Fragment 3.16
// The else body at lines 39 and 40 of that method relies on a locally declared variable 
//, newest. Redesign that clause to avoid use of any local variable.
{
    
}
class CircularlyLinkedList<integer>{
    private static class Node<integer>{
        private integer element;
        private Node<integer> next;
        public Node(integer e, Node<integer> n){
            element = e;
            next = n;
        }
        public integer getElement() {return element;}
        public Node<integer> getNext() {return next;}
        public void setNext(Node<integer> n) {next = n;}
    }
    private Node<integer> tail = null; 
    private int size = 0;
    public CircularlyLinkedList() { }
    public int size() { return size; }
    public boolean isEmpty() { return size == 0; }
    public integer first() { 
        if (isEmpty()) return null;
        return tail.getNext().getElement(); 
    }
    public integer last() {
        if (isEmpty()) return null;
        return tail.getElement(); 
    }
    public void rotate() {
        if (tail != null)
        tail = tail.getNext();
    }
    public void addFirst(integer e) {
        if (size == 0) {
            tail = new Node<>(e, null);
            tail.setNext(tail);
        } 
        else {
        tail.setNext(new Node<>(e, tail.getNext()));
        }
        size++;
    }
    public void addLast(integer e) {
        addFirst(e);
        tail = tail.getNext();
    }
    public integer removeFirst(){
        if (isEmpty()) return null;
        Node<integer> head = tail.getNext();
        if (head == tail) {
            tail = null; size--;}
        else {
            tail.setNext(head.getNext()); size --;}
        return head.getElement( );
    }
}
