public class CH2EX20 
// Assume that we change the CreditCard class (see Code Fragment 1.5) so that instance variable balance 
// has private visibility, but a new protected method is added, with signature setBalance(). Show how to
// properly implement the method PredatoryCreditCard.processMonth() in this setting
{
    public static void main(String[] args)
    {
        CreditCard20[] wallet = new CreditCard20[3];
        wallet[0] = new CreditCard20("John Bowman", "California Savings", "5391 0375 9387 5309", 5000);
        wallet[1] = new CreditCard20("John Bowman", "California Federal", "3485 0399 3395 1954", 3500);
        wallet[2] = new CreditCard20("John Bowman", "California Finance","5391 0375 9387 5309", 2500, 300);
        for (int val = 1; val <= 16; val++)
        {
            wallet[0].charge(3 * val);
            wallet[1].charge(2 * val);
            wallet[2].charge(val);
        }
        for(CreditCard20 card : wallet)
        {
            CreditCard20.printSummary(card);
            while (card.getBalance() > 200.0)
            {
                card.makePayment(200);
                System.out.print("\nNew balance = " + card.getBalance());
            }
        }
        PredatoryCreditCard20 test = new PredatoryCreditCard20("Meggie vd Oever", "ING Savings", "2345 2345 2345 2345", 2000, 1000, 10);
        for (int x = 0; x < 10; x++)
            test.charge(50);
        test.processMonth();
        CreditCard20.printSummary(test);
    }
}
class CreditCard20
{
    // Instance variables:
    private String customer;
    private String bank;
    private String account;
    private int limit;
    private double balance;
    // Constructors:
    public CreditCard20(String cust, String bk, String acnt, int lim, double initialBal)
    {
        customer = cust;
        bank = bk;
        account = acnt;
        limit = lim;
        balance = initialBal;
    }
    public CreditCard20(String cust, String bk, String acnt, int lim)
    {
        this(cust,bk,acnt,lim, 0.0);
    }
    // accerssor methods
    public String getCustomer() { return customer; }
    public String getBank() {return bank;}
    public String getAccount() {return account;}
    public int getLimit() {return limit;}
    public double getBalance() {return balance;}
    protected void setBalance(double newBalance) 
    { 
        balance = newBalance;
    }
    //update methods 
    public boolean charge(double price)
    {
        if(price + balance > limit)
        return false;
        balance += price;
        return true;
    }
    public void makePayment(double amount)
    {
        if (amount < 0)
        balance = amount;
        else
        balance -= amount;
    }
    //Utility method to print a card's information
    public static void printSummary(CreditCard20 card)
    {
        System.out.print("\nCustomer = " + card.customer);
        System.out.print("\nBank = " + card.bank);
        System.out.print("\nAccount = " + card.account);
        System.out.print("\nBalance = " + card.balance);
        System.out.print("\nLimit = " + card.limit);
    }
    public boolean updateLimit (int lim)
    {
        if(lim < balance)
        return false; 
        limit = lim;
        return true;
    }
}
class PredatoryCreditCard20 extends CreditCard20
    {
        private double apr;
        int countCharge = 0;
        public PredatoryCreditCard20(String cust, String bk, String acnt, int lim, double initialBal, double rate)
        {
            super(cust,bk,acnt,lim,initialBal);
            apr = rate;
        }
        public void processMonth()
        {
            if ( super.getBalance() > 0)
            {
                double monthlyFactor = Math.pow(1 + apr, 1.0 / 12);
                double b = super.getBalance() * monthlyFactor;
                super.setBalance(b);
            }
            if (countCharge >= 10)
            {
                double b = super.getBalance() + (1 * Math.floor(countCharge / 10));
                super.setBalance(b);
            }
        }
        public boolean charge(double price)
        {
            boolean isSuccess = super.charge(price);
            if (isSuccess)
                countCharge = countCharge + 1;
            else
            {
                double b = getBalance() + 5;
                super.setBalance(b);
            }
            return isSuccess;
        }
    }   
