import java.util.Scanner;
public class CH1EX3 
// Write a short Java method, isMultiple, that takes two long values, n and m,
// and returns true if and only if n is a multiple of m, that is
// n=mi for some integer i.
{
    //Long, Long -> String
    // isMultiple takes two long values n and m, and returns true if and only if
    // n is mutliple of m
    // given: 16, 4; expect: true
    // given: 23, 4; expect: false
    public static void main(String[] args)
    {
        System.out.print(isMultiple());
    }
    public static boolean isMultiple()
    {
        long n, m;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a long value >>");
        n = input.nextLong();
        System.out.print("Enter another long value >>");
        m = input.nextLong();
        input.close(); 
        // use the modulo (also called remainder) function, %, to compute wether it is a multiple
        if (n % m == 0)
        return true;
        else
        return false;
    }
}
