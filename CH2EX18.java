public class CH2EX18 
// The PredatoryCreditCard class provides a processMonth() method that models the completion of a monthly cycle
// Modify the class so that once a customer has made ten calls to charge during a month, each additional
// call to that method in the current month results in an additional $1 surcharge.
{
    public static void main(String[] args)
    {
        CreditCard[] wallet = new CreditCard[3];
        wallet[0] = new CreditCard("John Bowman", "California Savings", "5391 0375 9387 5309", 5000);
        wallet[1] = new CreditCard("John Bowman", "California Federal", "3485 0399 3395 1954", 3500);
        wallet[2] = new CreditCard("John Bowman", "California Finance","5391 0375 9387 5309", 2500, 300);
        for (int val = 1; val <= 16; val++)
        {
            wallet[0].charge(3 * val);
            wallet[1].charge(2 * val);
            wallet[2].charge(val);
        }
        for(CreditCard card : wallet)
        {
            CreditCard.printSummary(card);
            while (card.getBalance() > 200.0)
            {
                card.makePayment(200);
                System.out.print("\nNew balance = " + card.getBalance());
            }
        }
        PredatoryCreditCard test = new PredatoryCreditCard("Meggie vd Oever", "ING Savings", "2345 2345 2345 2345", 2000, 1000, 10);
        for (int x = 0; x < 10; x++)
            test.charge(50);
        test.processMonth();
        CreditCard.printSummary(test);
    }
}
class CreditCard
{
    // Instance variables:
    private String customer;
    private String bank;
    private String account;
    private int limit;
    protected double balance;
    // Constructors:
    public CreditCard(String cust, String bk, String acnt, int lim, double initialBal)
    {
        customer = cust;
        bank = bk;
        account = acnt;
        limit = lim;
        balance = initialBal;
    }
    public CreditCard(String cust, String bk, String acnt, int lim)
    {
        this(cust,bk,acnt,lim, 0.0);
    }
    // accerssor methods
    public String getCustomer() { return customer; }
    public String getBank() {return bank;}
    public String getAccount() {return account;}
    public int getLimit() {return limit;}
    public double getBalance() {return balance;}
    //update methods 
    public boolean charge(double price)
    {
        if(price + balance > limit)
        return false;
        balance += price;
        return true;
    }
    public void makePayment(double amount)
    {
        if (amount < 0)
        balance = amount;
        else
        balance -= amount;
    }
    //Utility method to print a card's information
    public static void printSummary(CreditCard card)
    {
        System.out.print("\nCustomer = " + card.customer);
        System.out.print("\nBank = " + card.bank);
        System.out.print("\nAccount = " + card.account);
        System.out.print("\nBalance = " + card.balance);
        System.out.print("\nLimit = " + card.limit);
    }
    public boolean updateLimit (int lim)
    {
        if(lim < balance)
        return false; 
        limit = lim;
        return true;
    }
}
class PredatoryCreditCard extends CreditCard
    {
        private double apr;
        int countCharge = 0;
        public PredatoryCreditCard(String cust, String bk, String acnt, int lim, double initialBal, double rate)
        {
            super(cust,bk,acnt,lim,initialBal);
            apr = rate;
        }
        public void processMonth()
        {
            if (balance > 0)
            {
                double monthlyFactor = Math.pow(1 + apr, 1.0 / 12);
                balance *= monthlyFactor;
            }
            if (countCharge >= 10)
                balance = balance + (1 * Math.floor(countCharge / 10));
            
        }
        public boolean charge(double price)
        {
            boolean isSuccess = super.charge(price);
            if (isSuccess)
                countCharge = countCharge + 1;
            else
                balance += 5;
            return isSuccess;
        }
    }   
