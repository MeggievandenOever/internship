public class CH3EX37 
// Write a class that maintains the top ten scores for a game application, implementing
// the add and remove mthods of Section 3.1.1, but using a singly linked list instead of 
// an array
{
    
}
class GameEntry {
    private String name;
    private int score;
    public GameEntry(String n, int s){
        name = n;
        score = s;
    }
    public String getName(){return name;}
    public int getScore(){return score;}
    public String toString(){
        return "(" + name + ", " + score + ")";
    }
}
// ScoreBoard is a singly linked list
class ScoreBoard<GameEntry> {
    private static class Node<GameEntry>{
        private GameEntry element;
        private Node<GameEntry> next;
        public Node(GameEntry e, Node<GameEntry> n){
            element = e;
            next = n;
        }
        public GameEntry getElement(){return element;}
        public Node<GameEntry> getNext(){return next;}
        public void setNext(Node<GameEntry> n) {next = n;}
    }
    private Node<GameEntry> head = null;
    private Node<GameEntry> tail = null;
    private int size = 0;
    public ScoreBoard() {}
    public int size() {return size;}
    public boolean isEmpty() {return size==0;}
    public GameEntry first() {
        if(isEmpty()) return null;
        return head.getElement();
    }
    public GameEntry last(){
        if(isEmpty()) return null;
        return tail.getElement();
    }
    public void addFirst(GameEntry e){
        head = new Node<>(e, head);
        if (size == 0)
            tail= head;
        size ++;
    }
    public void addLast(GameEntry e){
        Node<GameEntry> newest = new Node<>(e,null);
        if(isEmpty())
            head = newest;
        else
            tail.setNext(newest);
        tail = newest;
        size ++;
    }
    public GameEntry removeFirst() {
        if(isEmpty()) return null;
        GameEntry answer = head.getElement();
        head = head.getNext();
        size --;
        if (size == 0)
            tail = null;
        return answer;
    }
    public void add(GameEntry e){
        int newScore = e.getScore();
    }
}