import java.util.Set;
import java.util.Scanner;
public class CH2EX14 
// Give an example of a Java code fragment that performs an array reference that is
// possibly out of bounds, and if it is out of bounds, the program catches that exeption
// and prints the following error message: "Don't try buffer overflow attacks in Java!"
{
    public static void main(String[] args)
    {
        String S = "NAME";
        Money[] moneyArray = new Money[10];
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a integer >>");
        int x = input.nextInt();
        input.close();
        for (int i = 0; i < x ; i++)
        {
            if (x > moneyArray.length)
            {
                System.out.print("Don't try buffer overflow attacks in Java!");
                break;
            }
            moneyArray[i] = new Money(i, S);
        }
    }
}
class Money
{
    private int money;
    private String name;
    public Money(int m, String n){
        money = m;
        name = n;
    }
    public int Money() {return money; }
    public String Name() {return name;}
}