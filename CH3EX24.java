public class CH3EX24 
// Write a Java mathod that takes two three-dimensional integer arrays and adds
// them componentwise 
{
    public static void main(String[] args)
    {
       int[][][] one = { { { 14, 2 }, { 3, 4 } },
       { { 5, 6 }, { 7, 81 } } };
       int[][][] two = { { { 5, 7 }, { 6, 55 } },
       { { 53, 9 }, { 74, 12} } };
       ToString3DArray(one);
       ToString3DArray(two);
       ToString3DArray(add3DArray(one, two));

    }
    public static void ToString3DArray(int[][][] array)
    {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    System.out.print(array[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
    public static int[][][] add3DArray(int[][][] one, int[][][] two)
    {
        int ar = one.length;
        int row = one[0].length;
        int colm = one[0][0].length;
        int[][][] temp = new int[ar][row][colm];
        for (int i = 0; i < one.length; i++){
            for (int j = 0; j < one[0].length; j++){
                for (int k = 0; k < one[0][0].length; k++){
                    temp[i][j][k] = one[i][j][k] + two[i][j][k];
                }
            }
        }
        return temp;
    }
}
