public class CH2EX21
// Write a program that consists of three classes, A, B, C, such that B extends 
// A and C extends B. Each class should define an instance variable named "x"
// (that is, each has its own variable name x). Describe a way for a method in C
// to access and set A's version of x to a given value, without changing B or C's version
{
    public static void main(String[] args)
    {
        C objectInC = new C(8, "Goodbye", 22.2);
        objectInC.printxInA();
        objectInC.printxInB();
        objectInC.printxInC();

        objectInC.setxInA(3);

        objectInC.printxInA();
        objectInC.printxInB();
        objectInC.printxInC();
    }
}
class A
{
    private int x;
    public A(int integer)
    {
        x = integer;
    }
    public int getxInA() {return x;}
    public void setxInA(int integer)
    {
        x = integer;
    }
    public void printxInA()
    {
        System.out.println("A's version of x " + x);
    }

}
class B extends A
{
    private String x;
    public B(int integer, String string)
    {
        super(integer);
        x = string;
    }
    public String getxInB() {return x;}
    public void setxInB(String string)
    {
        x = string;
    }
    public void printxInB()
    {
        System.out.println("B's version of x: " + x);
    }
}
class C extends B
{
    private double x;
    public C(int integer, String string, double doubleNumber)
    {
        super(integer, string);
        x = doubleNumber;
    }
    public double getxInC() {return x;}
    public void setxInC(double doubleNumber)
    {
        x = doubleNumber;
    }
    public void setxInA(int integer)
    {
        super.setxInA(integer);
    }
    public int getxInA() {return super.getxInA();}
    public void printxInC()
    {
        System.out.println("C's version of x: " + x);
    }
}