import java.util.Scanner;
public class CH1EX4 
// Write a short Java mathod, isEven, that takes an int i and returns true if and only 
// if i is even. Your method cannot use the multiplication, modulus, or division operators
{
    // Integer -> String
    // isEven takes an integer and returns true if and only if the integer is even.
    // given: 6; expect: true
    // given: 23; expect: false
    public static void main(String[] args)
    {
        int i;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer >>");
        i = input.nextInt();
        input.close();
        if (isEven(i) == true)
        System.out.print("The number entered is even!");
        else
        System.out.print("The number entered is uneven!");
    }
    public static boolean isEven(int i)
    {
        boolean even;
        while(true)
        {
            if (i == 0)
            {
                even = true;
                break;
            }
            else if (i == -1)
            {
                even = false;
                break;
            }
            i = i - 2;
        }
        return even;
    }
}
