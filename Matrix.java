import java.util.Scanner;
public class Matrix {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        int one[][] = new int[2][2];
        int two[][] = new int[2][2];
        for (int x = 0; x<4; x++){
            System.out.println("Enter element " + x + 1 + "of matrix 1");
            if (x == 0)
                one[0][0] = input.nextInt();
            if (x == 1)
                one[0][1] = input.nextInt();
            if (x == 2)
                one[1][0] = input.nextInt();
            if (x == 3)
                one[1][1] = input.nextInt();
        }
        for (int x = 0; x<4; x++){
            System.out.println("Enter element " + x + 1 + "of matrix 2");
            if (x == 0)
                one[0][0] = input.nextInt();
            if (x == 1)
                one[0][1] = input.nextInt();
            if (x == 2)
                one[1][0] = input.nextInt();
            if (x == 3)
                one[1][1] = input.nextInt();
        }
        input.close();
        int add[][] = new int[2][2];
        for (int x = 0; x< 4; x++){
            if (x == 0)
                add[0][0] = one[0][0] + two[0][0];
            if (x == 1)
                add[0][1] = one[0][1] + two[0][1];
            if (x == 2)
                add[1][0] = one[1][0] + two[1][0];
            if (x == 3)
                add[1][1] = one[1][1] + two[1][1];
        } 
        for (int x = 0; x< 4; x++){
            System.out.println("Addition of matrices is:");
            System.out.println("\n" + add[0][0] + " " + add[0][1]);
            System.out.println("\n" + add[1][0] + " " + add[1][1]);
        }  
        int prod[][] = new int[2][2];
        for (int x = 0; x< 4; x++){
            if (x == 0)
                prod[0][0] = (one[0][0] * two[0][0]) + (one[0][1] * two[1][0]);
            if (x == 1)
                prod[0][1] = (one[0][0] * two[0][1]) + (one[0][1] * two[1][1]);
            if (x == 2)
                prod[1][0] = (one[1][0] * two[0][0]) + (one[1][1] * two[1][0]);
            if (x == 3)
                prod[1][1] = (one[1][0] * two[1][0]) + (one[1][1] * two[1][1]);
        } 
        for (int x = 0; x< 4; x++){
            System.out.println("Product of matrices is:");
            System.out.println("\n" + prod[0][0] + " " + prod[0][1]);
            System.out.println("\n" + prod[1][0] + " " + prod[1][1]);
        }      

    }
}
