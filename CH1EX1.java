import java.util.Scanner;
public class CH1EX1
// Write a short Java method, inputBaseTypes, that inputs a different value of each
// of each base type from the standard input device and prints it back to the standard
//output device 
{
    // Boolean + byte + short + integer + long + float + double -> String
    // inputAllBaseTypes inputs a different value of each base type from the standard
    // input device and prints it back to the standard output device
    // given: true, expect : Succes: true
    // given: 44.5, expect: Succes: 44.5
    public static void main(String[] args)
    {
        inputAllBaseTypes();
    }
    public static void inputAllBaseTypes()
    {
        boolean boolValue;
        byte byteValue;
        short shortValue;
        int integerValue;
        long longValue;
        float floatValue;
        double doubleValue;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a boolean >>");
        boolValue = input.nextBoolean();
        System.out.print("Succes: " + boolValue);
        System.out.print("Enter a byte >>");
        byteValue = input.nextByte();
        System.out.print("Succes: " + byteValue);
        System.out.print("Enter a short >>");
        shortValue = input.nextShort();
        System.out.print("Succes: " + shortValue);
        System.out.print("Enter a integer >>");
        integerValue = input.nextInt();
        System.out.print("Succes: " + integerValue);
        System.out.print("Enter a long >>");
        longValue = input.nextLong();
        System.out.print("Succes: " + longValue);
        System.out.print("Enter a float >>");
        floatValue = input.nextFloat();
        System.out.print("Succes: " + floatValue);
        System.out.print("Enter a double >>");
        doubleValue = input.nextDouble();
        System.out.print("Succes: " + doubleValue);
        input.close();
    }

}
